# Utilidad para ELK

Su objetivo es tener un ambiente ELK local que incluye:

- Kibana (https://www.elastic.co/guide/en/kibana/7.16/index.html)
- Elasticsearch (https://www.elastic.co/guide/en/elasticsearch/reference/7.16/index.html)
- APM Server (https://www.elastic.co/guide/en/apm/guide/7.16/index.html)
- Logstash (https://www.elastic.co/guide/en/logstash/7.16/index.html)
- Filebeat (https://www.elastic.co/guide/en/beats/filebeat/7.16/index.html)

Para su configuración se usa ```docker-compose``` y se tienen los siguientes archivos de configuración

---

## ```.env```

```sh
# Define la versión del stak (todos los componentes usarán esta versión)
TAG=7.16.3 
# Reemplace aquí el directorio de logs que desea monitorear con filebeat
DIRLOGS=/Users/eartunduaga/projects/project-coupons/makeitrain/storage/logs
```

--- 


## ```docker-compose.yml```

Define la infraestructura de ELK en un grupo de contenedores con las siguientes caracteristicas:

- Elasticsearch:
    - Expone los puertos 9200 y 9300

- APM:
    - Expone el puertos 8200
    - Toma la configuración desde el archivo ```./apm-server/apm-server.docker.yml```

- Kibana:
    - Expone el puertos 5601
    - Se conecta a http://elasticsearch:9200

- Logstash:
    - Expone el puertos 5044
    - Toma la configuración de pipeline desde el archivo ```./logstash/pipeline/logstash.conf```
    - Soporta hot reload del pipeline

- Filebeat:
    - Toma la configuración de pipeline desde el archivo ```./filebeat/filebeat.yml```

Para su instalación debe ingresar a la carpeta ```elk``` y ejecutar el siguiente comando.

```bash
docker-compose up -d
```

---


## ```./apm-server/apm-server.docker.yml``` 

Contiene las configuraciones de apm-server, aqui se define el puerto de escucha y el destino de los logs

- host y puerto donde se escucharán peticiones
    ```yml
    apm-server:
      host: "0.0.0.0:8200"
    ```

- Host y puerto de Elasticsearch,  se usa el valor de host ```elasticsearch``` ya que es el nombre del contenedor en ```docker-compose.yml```
    ```yml
    output.elasticsearch:
      hosts: ["elasticsearch:9200"]
    ```

--- 

## ```./filebeat/filebeat.yml```

Contiene las configuraciones de filebeat, aqui se define el tipo de entrada y el destino.

- Definición de entradas, por defecto archivos .log en /tmp/logs (esta ruta es un volumen en ```docker-compose```)

    ```yml
    filebeat.inputs:
    - type: filestream
      paths:
        - /tmp/logs/*.log
      parsers:
        - ndjson:
          keys_under_root: true
          overwrite_keys: true
          add_error_key: true
          expand_keys: true
    ```

- Definición de salida, por defecto se envia a logstash ( se usa el valor de host ```logstash``` ya que es el nombre del contenedor en ```docker-compose.yml```)

  ```yml
  output.logstash:
    hosts: ["logstash:5044"]
  ```

--- 

## ```./logstash/pipeline/logstash.conf```

Contiene las configuraciones de pipeline de logstash, aqui se define la entrada, filtros y salida.

- En la definición de entrada por defecto se tiene beats por puerto 5044 (al que apunta filebeat) y se usa codec json (cambiar el codec que aplique para su proyecto)

    ```
    input {
        beats {
            port => 5044
            codec => json
        }
    }
    ```

- En la definición de filtros se definen para gestión de logs tipo json de zap para golang

    ```
    filter {
        json {
            source => 'message'
            skip_on_invalid_json => true
        }
        mutate {
            remove_field => ['agent', 'ecs', 'log', 'tags']
            add_field => { 
                'app' => 'my-app'
                'app_version' => '1.0'
                'platform' => 'go'
            }
            rename => {
                'msg' => 'message'
            }
        }
        if [status] {
            if [status] >= 200 and [status] <= 299 {
                drop {}
            }
        }
        date {
            match => [ 'ts', 'ISO8601']
        }
    }
    ```

- En la definición de salida se define enviar logs a elasticsearch, se usa el valor de host ```elasticsearch``` ya que es el nombre del contenedor en ```docker-compose.yml```

    ```
    output {
        elasticsearch {
            hosts => ['http://elasticsearch:9200']
            index => 'local-index'
            ilm_enabled => "false"
            ilm_policy => ""
        }
    }
    ```