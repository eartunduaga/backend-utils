#!/bin/bash

BLACK='\e[0;30m'
RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
BLUE='\e[0;34m'
PURPLE='\e[0;35m'
CYAN='\e[0;36m'
GREY='\e[0;37m'

NC='\e[0m'

function read_noempty() {
    value=""
    while [ "$value" = "" ]
    do
        echo -n "$2"
        read value 
    done
    eval "$1=$value"
}

function read_withdefault() {
    var="$1"
    type="$2"
    defv="$3"
    lbl="$4"
    value=""
    while [ "$value" = "" ]
    do
        echo -n $lbl
        read value
        if [ "$value" = "" ] && [ "$defv" != "" ]
        then 
            value=$defv
        fi
    done

    eval "$1=$value"


    read value
    if [ "$value" = "" ]
    then
        value=$2
    fi
    echo $value
}

function progress_in_text() {
    text=$1
    perc=$2
    add_text=$3
    text_len=${#text}
    add_text_len=${#add_text}
    limit=$(echo "$text_len $2" | awk '{print int($1 * $2)}')
    perc=$(echo "$2" | awk '{print(int($1 * 100))}')
    if [ $perc -gt 100 ]; then 
        perc=100
    fi
    cols=$(stty size | awk '{print $2}')
    cols=$(($cols - $add_text_len - 15)) 

    txt="${GREEN}"
    applied=0
    for pos in $(seq 0 $cols)
    do
        if [ $pos -ge $limit ] && [ $applied -eq 0 ]
        then
            applied=1
            txt="$txt${GREY}"
        fi
        if [ $pos -le $text_len ]
        then 
            txt="$txt${text:$pos:1}"
        else 
            txt="$txt "
        fi
        
    done
    printf "\r$txt $add_text $perc%%${NC}"
}

function progress() {
    cols=$(stty size | awk '{print $2}')
    cols=$(($cols - 17))  
    limit=$(echo "$cols $1" | awk '{print(int($1 * $2))}')
    if [ $limit -gt $cols ]
    then 
        limit=$cols
    fi
    perc=$(echo "$1" | awk '{print(int($1 * 100))}')
    if [ $perc -gt 100 ]
    then 
        perc=100
    fi
    txt=""
    for pos in $(seq 1 $cols)
    do
        if [ $pos -lt $limit ]
        then 
            txt="$txt#"
        else
            txt="$txt "
        fi
    done
    printf "\r${GREEN}[$txt] ${NC}$perc%%"
}

function get_args() {
    ind=0 && defs="" && CUROPTION="" && CURVARVALUE="" && CURVARNAME=""
    for opt in $@; do
        ((ind+=1))
        [ $ind -eq 1 ] && defs=$(echo $opt | tr ';' ' ') && continue
        if [[ $opt =~ ^[-][-]?[a-zA-Z0-9-]+$ ]]; then
            computed=0
            for def in $defs; do 
                DEF_OPTIONS=$(echo $def | tr ':' ' ' | awk '{print $1}' | tr '|' ' ')
                DEF_VARNAME=$(echo $def | tr ':' ' ' | awk '{print $2}') 
                for option in $DEF_OPTIONS; do
                    [ "$option" == "$opt" ] && computed=1 && break
                done
                [ $computed -eq 0 ] && continue
                [ "$CURVARNAME" != "" ] && [ "$CURVARVALUE" = "" ] && eval "$CURVARNAME=$CURVARNAME"
                CURVARNAME=$DEF_VARNAME && CURVARVALUE=$opt && CUROPTION=$opt
                eval "$CURVARNAME=\"$CURVARVALUE\""
                break
            done
        else 
            if [ "$CURVARNAME" != "" ]; then
                [ "$CURVARVALUE" == "$CUROPTION" ] && CURVARVALUE=""
                CURVARVALUE=$(echo "$CURVARVALUE $opt" | xargs)
                eval "$CURVARNAME=\"$CURVARVALUE\""
            fi
        fi
    done 
}

function check_mysql_connection() {
    if [ "$MYSQL" = "" ]; then 
        printf "${RED}Error checking mysql connection: Variable 'MYSQL' is not defined\n"
        exit
    fi

    $MYSQL -e "SELECT NOW()" > /dev/null 2>&1
    
    if [ $? -gt 0 ]; then
        printf "${RED}Error checking mysql connection: ${NC}There was an error connecting with MySQL server\n"
        exit
    fi
}
