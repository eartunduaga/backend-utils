#!/bin/bash

BASEDIR=$(cd "$(dirname "$0")" ; pwd -P )
cd $BASEDIR
. variables.sh
. ../utils.sh

MYSQLDUMP=$TMP_LOCAL_MYSQLDUMP
MYSQL=$TMP_LOCAL_MYSQL

get_args "-d|--databases:DATABASES;--to-test:TOTEST;-h|--help:HELP" $@

if [ "$HELP" != "" ] || [ "$DATABASES" = "" ] || [ "$DATABASES" = "-d" ]; then
    SCRIPTNAME=$(basename $0)
    printf "\nUsage: ${GREEN}$SCRIPTNAME ${CYAN} -d|--databases db1 db2 ... [ -h|--help ] ${NC}\n\n"
    printf "${CYAN} --d|--databases db1 db2 ...${NC}: Defines databases to restore locally ${RED}(required)${NC}\n"
    printf "                  ${YELLOW}-d DEFAULT${NC}: For default values ($DEFAULT_DATABASES)\n\n"
    printf "               ${CYAN}[ --to-test ]${NC}: Restore to test databases (database names with ${PURPLE}_test ${NC}sufix) \n\n"
    printf "               ${CYAN}[ -h|--help ]${NC}: Show this help\n\n"
    exit
fi

check_mysql_connection

if [ "$DATABASES" = "DEFAULT" ] || [ "$DATABASES" = "-d" ] ; then
    DATABASES=$DEFAULT_DATABASES
fi

DEST_SUFIX=""
if [ "$TOTEST" != "" ]; then
    DEST_SUFIX="_test"
fi

for db in $DATABASES; do
    [ ! -d $DUMPFOLDER/$db ] && printf "${RED}Error: ${NC}$DUMPFOLDER/$db does not exist\n" && continue
    destdb=$db$DEST_SUFIX
    $MYSQL -e "DROP DATABASE IF EXISTS $destdb;" 2>/dev/null
    $MYSQL -e "CREATE DATABASE $destdb CHARACTER SET utf8 COLLATE utf8_unicode_ci;" 2>/dev/null
    for file in $(ls $DUMPFOLDER/$db/*.sql); do
        printf "Restoring ${GREEN}$file${NC} to ${CYAN}$destdb${NC}\n"
        $MYSQL $destdb < $file 2>/dev/null
    done
done