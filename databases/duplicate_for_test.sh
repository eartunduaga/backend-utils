#!/bin/bash

BASEDIR=$(cd "$(dirname "$0")" ; pwd -P )
cd $BASEDIR
. variables.sh
. ../utils.sh

MYSQLDUMP=$TMP_LOCAL_MYSQLDUMP
MYSQL=$TMP_LOCAL_MYSQL

get_args "-d|--databases:DATABASES;--with-data:WITHDATA;-h|--help:HELP" $@

if [ "$HELP" != "" ] || [ "$DATABASES" = "" ] || [ "$DATABASES" = "-d" ]; then
    SCRIPTNAME=$(basename $0)
    printf "\nUsage: ${GREEN}$SCRIPTNAME ${CYAN} -d|--databases db1 db2 ... [ --with-data ] [ -h|--help ] ${NC}\n\n"
    printf "${CYAN} --d|--databases db1 db2 ...${NC}: Defines databases to be duplicated locally ${RED}(required)${NC}\n"
    printf "                  ${YELLOW}-d DEFAULT${NC}: For default values ($DEFAULT_DATABASES)\n\n"
    printf "             ${CYAN}[ --with-data ]${NC}: Duplicate databases with data (without data if option is skiped)\n\n"
    printf "               ${CYAN}[ -h|--help ]${NC}: Show this help\n\n"
    exit
fi

check_mysql_connection

if [ "$DATABASES" = "DEFAULT" ] || [ "$DATABASES" = "-d" ] ; then
    DATABASES=$DEFAULT_DATABASES
fi

printf "${CYAN}Using databases: ${NC}$DATABASES\n"

MYSQLDATAFLAG="--no-data" && WITHDATAMSG="without data"
[ "$WITHDATA" != "" ] && MYSQLDATAFLAG="" && WITHDATAMSG="with data"

local_databases=$($MYSQL -e "SHOW DATABASES\G" 2>/dev/null | grep "Database: " | grep -v information_schema | grep -v mysql | grep -v performance_schema | grep -v _test)
local_databases="${local_databases//Database: /}"

for db in $DATABASES; do
    is_valid=0
    for ldb in $local_databases; do
        [ "$ldb" = "$db" ] && is_valid=1
    done
    [ $is_valid -eq 0 ] && printf "${RED}Error: ${NC}$db is not valid\n" && continue
    db_test=$db"_test"
    $MYSQL -e "CREATE DATABASE IF NOT EXISTS $db CHARACTER SET utf8 COLLATE utf8_unicode_ci;" 2>/dev/null
    $MYSQL -e "DROP DATABASE IF EXISTS $db_test" 2>/dev/null
    $MYSQL -e "CREATE DATABASE $db_test CHARACTER SET utf8 COLLATE utf8_unicode_ci;" 2>/dev/null
    printf "Downloading $WITHDATAMSG ${GREEN}$db ${NC}to ${CYAN}$db.sql ${NC}...\n"
    $MYSQLDUMP $MYSQLDATAFLAG $db > $db.sql 2>/dev/null
    sed -i '' 's/DEFINER=[^*]*\*/\*/g' $db.sql
    printf "Loading ${CYAN}$db.sql ${NC}to ${GREEN}$db_test ${NC}...\n"
    $MYSQL $db_test < $db.sql 2>/dev/null
    rm -f $db.sql
done
