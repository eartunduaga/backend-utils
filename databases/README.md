# Utilidad para bases de datos

Su objetivo es controlar la descarga de base de datos de desarrollo a local.

Copiar el archivo ```variables.sh.example```, renombrarlo a ```variables.sh``` y configurar las variables necesarias.

--- 

## ```remote_to_sql.sh``` 

- Permite descargar base de datos de desarrollo a dumps .sql con la estructura: ```dumps/{DATABASE}/{TABLE}.sql```

- En el archivo ```dumps_filters/{DATABASE}/no_data.txt``` se definen las tablas de las cuales solo se requiere descargar la estructura. 

- Si se usa la opción ```--no-data``` se descarga solo la estructura de la base de datos. (Como si se definieran todas las tablas en los archivos ```dumps_filters/{DATABASE}/no_data.txt```)

- En el archivo ```dumps_filters/{DATABASE}/skip.txt``` se definen las tablas que no se requiere descargar

- En los archivos ```no_data.txt```y ```skip.txt``` se puede desactivar una linea comentando la linea con ```';'```

- Este script recibe diferentes opciones como argumentos, para más detalles usar la opcion ```-h``` o ```--help```

```bash
# Esto descargará las bases de datos muy y sgi en dumps .sql 
./remote_to_sql.sh -d muy sgi

# Esto descargará las bases de datos definidas en la variable DEFAULT_DATABASES del archivo variables.sh
./remote_to_sql.sh -d DEFAULT

# Para mostrar ayuda
./remote_to_sql.sh -h
```

## ```sql_to_local.sh```

- Permite cargar los dumps .sql ```dumps/{DATABASE}/{TABLE}.sql``` en la base de datos local.

- Si se desea cargar a las bases de datos de test usar la opción ```--to-test```

- Este script recibe diferentes opciones como argumentos, para más detalles usar la opcion ```-h``` o ```--help```

```bash
# Esto cargará los dumps .sql de las bases de datos muy y sgi en el motor de base de datos local
./sql_to_local.sh -d muy sgi

# Esto cargará los dumps .sql de las bases de datos definidas en la variable DEFAULT_DATABASES en el motor de base de datos local
./sql_to_local.sh -d DEFAULT

# Para mostrar ayuda
./sql_to_local.sh -h
```

## ```duplicate_for_test.sh```

- Crea una copia de la estructura de las bases de datos en el motor de base de datos con el sufijo "_test"

- Usando la opción ```--with-data``` se crea la copia con datos.

- Este script recibe diferentes opciones como argumentos, para más detalles usar la opcion ```-h``` o ```--help```

```bash
# Copia la base de datos muy a la base de datos muy_test
./duplicate_for_test.sh -d muy

# Copia la base de datos las bases de datos definidas en la variable DEFAULT_DATABASES a la base de datos con prefijo _test 
./duplicate_for_test.sh -d DEFAULT

# Para mostrar ayuda
./duplicate_for_test.sh -h
```

## ```skeema_push_local.sh```

- Sincroniza la estructura de la BD usando el proyecto legacy-migrations

- Si se desea sincronizar a las bases de datos de test usar la opción ```--to-test```

- Este script recibe diferentes opciones como argumentos, para más detalles usar la opcion ```-h``` o ```--help```

```bash
# Sincroniza la estructura de la BD muy definida en legacy-migrations con la base de datos muy 
./skeema_push_local.sh -d muy

# Sincroniza la estructura de las bases de datos definidas en el proyecto legacy-migrations definido en la variable LEGACY_MIGRATIONS_PATH
./skeema_push_local.sh -d DEFAULT

# Para mostrar ayuda
./skeema_push_local.sh -h
```