#!/bin/bash

BASEDIR=$(cd "$(dirname "$0")" ; pwd -P )
cd $BASEDIR
. variables.sh
. ../utils.sh

MYSQLDUMP=$TMP_REMOTE_MYSQLDUMP
MYSQL=$TMP_REMOTE_MYSQL

get_args "-d|--databases:DATABASES;--no-data:NODATA;-h|--help:HELP" $@

if [ "$HELP" != "" ] || [ "$DATABASES" = "" ] || [ "$DATABASES" = "-d" ]; then
    SCRIPTNAME=$(basename $0)
    printf "\nUsage: ${GREEN}$SCRIPTNAME ${CYAN} -d|--databases db1 db2 ... [ --no-data ] [ -h|--help ] ${NC}\n\n"
    printf "${CYAN} --d|--databases db1 db2 ...${NC}: Defines remote databases to be backed up ${RED}(required)${NC}\n"
    printf "                  ${YELLOW}-d DEFAULT${NC}: For default values ($DEFAULT_DATABASES)\n\n"
    printf "               ${CYAN}[ --no-data ]${NC}: Apply ${PURPLE}--no-data ${NC}to mysqldump (overrides ${GREEN}$FILTERFOLDER/DBNAME/no_data.txt ${NC}values)\n\n"
    printf "               ${CYAN}[ -h|--help ]${NC}: Show this help\n\n"
    exit
fi

check_mysql_connection

if [ "$DATABASES" = "DEFAULT" ] || [ "$DATABASES" = "-d" ] ; then
    DATABASES=$DEFAULT_DATABASES
fi

printf "${CYAN}Using databases: ${NC}$DATABASES\n"

MYSQLDATAFLAG=""
[ "$NODATA" != "" ] && MYSQLDATAFLAG="--no-data"

remote_databases=$($MYSQL -e "SHOW DATABASES\G" 2>/dev/null | grep "Database: " | grep -v information_schema | grep -v mysql | grep -v performance_schema | grep -v _test)
remote_databases="${remote_databases//Database: /}"

for db in $DATABASES; do
    is_valid=0
    for ldb in $remote_databases; do
        [ "$ldb" = "$db" ] && is_valid=1
    done
    
    if [ $is_valid -eq 0 ]; then
        printf "${RED}Error: ${NC}database ${GREEN}$db ${NC}does not exist\n"
        continue
    fi
    
    TMP_FILE=.$db.tmp
    tables=$($MYSQL -e "SELECT CONCAT(TABLE_NAME, ',', ROUND((DATA_LENGTH) / 1024 )) AS tabledata FROM information_schema.TABLES WHERE TABLE_SCHEMA = '$db' AND TABLE_TYPE = 'BASE TABLE' ORDER BY (DATA_LENGTH + INDEX_LENGTH) DESC\G" 2>/dev/null | grep "tabledata")
    tabledata="${tables//tabledata: /}"
    
    DB_DUMPFOLDER=$DUMPFOLDER/$db
    [ ! -d $DB_DUMPFOLDER ] && mkdir -p $DB_DUMPFOLDER
    
    rm -f $DB_DUMPFOLDER/*.sql
    
    no_data_file=$FILTERFOLDER/${db}/no_data.txt
    if [ ! -f $no_data_file ]; then
        mkdir -p $(dirname $no_data_file)
        touch $no_data_file
    fi
    
    skip_file=$FILTERFOLDER/${db}/skip.txt
    if [ ! -f $skip_file ]; then
        mkdir -p $(dirname $skip_file)
        touch $skip_file
    fi
    
    for data in $tabledata; do
        table=$(echo $data | tr ',' ' ' | awk '{print $1}')
        size=$(echo $data | tr ',' ' ' | awk '{print $2}')
        skiptable=$(cat $skip_file | grep '^[^;]' | grep -w $table | wc -l | sed 's/ //g')
        [ $skiptable -eq 1 ] && continue
        
        addarg=""
        no_data=$(cat $no_data_file | grep '^[^;]' | grep -w $table | wc -l | sed 's/ //g')
        echo_data="with data ${size} KB"
        if [ $no_data -eq 1 ] || [ "$MYSQLDATAFLAG" != "" ]; then
            addarg="--no-data"
            echo_data="without data"
        fi
        message="Dumping $db.$table to $DB_DUMPFOLDER/$table.sql"
        
        DESTFILE=$DB_DUMPFOLDER/$table.sql
        $MYSQLDUMP --databases $db --tables $table $addarg 2>/dev/null > $DESTFILE &
        pid=$!
        while [ $no_data -eq 0 ] && [ "$(ps -p $pid >/dev/null 2>&1 && echo $?)" = "0" ]; do
            cursize=$(du -k $DESTFILE | awk '{print $1}')
            perc=$(echo "$cursize $size" | awk '{print $1/$2 }')
            progress_in_text "$message" $perc "${BLUE}($echo_data)${NC}"
            sleep 0.1
        done
        progress_in_text "$message" 1 "${BLUE}($echo_data)${NC}"
        echo
    done
done