#!/bin/bash

BASEDIR=$(cd "$(dirname "$0")" ; pwd -P )
cd $BASEDIR
. variables.sh
. ../utils.sh

MYSQLDUMP=$TMP_LOCAL_MYSQLDUMP
MYSQL=$TMP_LOCAL_MYSQL
SKEEMA_TEMPLATE=$BASEDIR/.skeema.template

LEGACY_DATABASES=$(find $LEGACY_MIGRATIONS_PATH -maxdepth 1 -mindepth 1 -type d -exec basename {} \; | grep -v ".git" | tr '\n' ' ')

get_args "-d|--databases:DATABASES;--to-test:TOTEST;-h|--help:HELP" $@

if [ "$HELP" != "" ] || [ "$DATABASES" = "" ] || [ "$DATABASES" = "-d" ]; then
    SCRIPTNAME=$(basename $0)
    printf "\nUsage: ${GREEN}$SCRIPTNAME ${CYAN} -d|--databases db1 db2 ...  [ -h|--help ] ${NC}\n\n"
    printf "${CYAN} --d|--databases db1 db2 ...${NC}: Defines databases schemas to be pushed locally using skeema ${RED}(required)${NC}\n"
    printf "                  ${YELLOW}-d DEFAULT${NC}: For default values ($LEGACY_DATABASES)\n\n"
    printf "               ${CYAN}[ --to-test ]${NC}: Push changes to test databases (database names with ${PURPLE}_test ${NC}sufix) \n\n"
    printf "               ${CYAN}[ -h|--help ]${NC}: Show this help\n\n"
    exit
fi

check_mysql_connection

if [ "$DATABASES" = "DEFAULT" ] || [ "$DATABASES" = "-d" ] ; then
    DATABASES=$DEFAULT_DATABASES
fi

DEST_SUFIX=""
if [ "$TOTEST" != "" ]; then
    DEST_SUFIX="_test"
fi

for db in $DATABASES; do
    dbname="$db"
    [ ! -d $LEGACY_MIGRATIONS_PATH/$dbname ] && printf "${RED}Error: ${NC}$LEGACY_MIGRATIONS_PATH/$dbname does not exist\n" && continue
    
    printf "Pushing ${GREEN}$dbname$DEST_SUFIX ${NC}changes ...\n"
    cd $LEGACY_MIGRATIONS_PATH/$dbname
    [ -f .skeema ] && rm .skeema
    export SKEEMA_DATABASE=$dbname$DEST_SUFIX
    export SKEEMA_HOST=$LOCAL_HOST
    export SKEEMA_PORT=$LOCAL_PORT
    export SKEEMA_USER=$LOCAL_USER
    export SKEEMA_PASS=$LOCAL_PASS
    envsubst '$SKEEMA_DATABASE,$SKEEMA_HOST,$SKEEMA_PORT,$SKEEMA_USER,$SKEEMA_PASS' < $SKEEMA_TEMPLATE > .skeema
    skeema push --allow-unsafe local
    #skeema diff local
    rm .skeema
done
