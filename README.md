# Backend Utils

Este proyecto se creó como repositorio de utilidades, scripts, documentación entre otros

## Reglas

-   Ser organizado en la nomenclatura y estructura de carpetas.
-   Documentar
-   Compartir conocimiento xD

--- 

# Utilidad para bases de datos

[databases/README.md](databases/README.md)

--- 

# Utilidad para stack ELK

[elk/README.md](elk/README.md)